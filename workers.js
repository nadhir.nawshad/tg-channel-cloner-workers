// This Script Handles Copy Old Messages. It copies 20 Messages in 1 minute.

var tg_bot_token = "6277558534:AAHDMhZCkwswi6_a2hfvypP8rkGNzYqentA"// Replace with Telegram Bot Token
var admin = "1191861093";

addEventListener('scheduled', event => {
    event.waitUntil(
        handleSchedule(event)
    )
})

async function handleSchedule(event) {
    var from_channel = await ENV.get("from_channel");
    var to_channel = await ENV.get("to_channel");
    var first_message_id = Number(await ENV.get("start_message_id"));
    var last_message = Number(await ENV.get("last_message_id"));
    var total_messages_to_clone = last_message - first_message_id;
    if (total_messages_to_clone < 20) {
        var end_message_id = last_message;
    } else {
        var end_message_id = first_message_id + 19;
    }
    var status = await ENV.get("status");
    if (status == "false") {
        console.log("Bot Idle!")
    } else if (first_message_id < last_message && status == "true") {
        try {
            for (var i = first_message_id; i < end_message_id + 1; i++) {
                var res = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/copyMessage?disable_web_page_preview=true&chat_id=" + to_channel + "&from_chat_id=" + from_channel + "&message_id=" + i, {
                    method: "GET",
                });
                var jsondata = await res.text()
                //var copydata = JSON.stringify(res);
                //var copydata1 = await JSON.parse(copydata);
                var loggingi = i;
                if (res.status == 429) {
                    console.log("Flood Wait at " + i + "\n\n" + jsondata);
                    break;
                }
            }
            if (res.ok || res.status == 400) {
                console.log("Logging Copy Message Data: " + end_message_id)
                await ENV.put("start_message_id", loggingi + 1);
                await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Cloned Status\nDone Message ID: " + end_message_id + "\nClone Upto: " + last_message + "\n\n" + jsondata, {
                    method: "GET",
                });
            } else if (res.status == 429) {
                await ENV.put("start_message_id", loggingi);
                await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Flood Wait at: " + loggingi + "\n\n" + jsondata, {
                    method: "GET",
                });
            } else {
                await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Bot Paused.\n\nSome Unknown Error Occurred\n\nError JSON: " + jsondata, {
                    method: "GET",
                });
                await ENV.put("status", "false");
            }
        } catch (e) {
            console.log(e);
        }
    } else if (first_message_id == last_message && status == "true") {
        console.log("Work Finished!");
        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Clone Finished.\n\nBot Stopped.", {
            method: "GET",
        });
        await ENV.put("status", "false");
    } else {
        console.log("Server Idle!")
        await ENV.put("status", "false");
        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Server Idle!", {
            method: "GET",
        });
    }
}


async function handleRequest(request) {
    var url = new URL(request.url);
    var path = url.pathname;
    var hostname = url.hostname;
    var from_channel = await ENV.get("from_channel");
    var to_channel = await ENV.get("to_channel");
    var first_message_id = Number(await ENV.get("start_message_id"));
    var end_message_id = await Number(first_message_id) + 15;
    var last_message = Number(await ENV.get("last_message_id"));
    var status = await ENV.get("status");
    if (path == '/') {
        var setwebhook = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/setWebhook?url=https://" + hostname + "/bot&drop_pending_updates=true&max_connections=100", {
            method: "GET",
        });
        if (setwebhook.ok) {
            return new Response("Web Hook Set", {
                headers: {
                    'content-type': 'text/html;charset=UTF-8',
                },
            });
        } else {
            return new Response("Unable to Set Web Hook, something went wrong.", {
                headers: {
                    'content-type': 'text/html;charset=UTF-8',
                },
            });
        }
    } else if (path == '/bot') {
        .
        var data = JSON.stringify(await request.json());
        var obj = JSON.parse(data);
        var status = await ENV.get("status");
        /*if (obj.hasOwnProperty('channel_post')) {
            if (obj.channel_post.sender_chat.id == from_channel) {
                var res = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/copyMessage?disable_web_page_preview=true&chat_id=" + to_channel + "&from_chat_id=" + from_channel + "&message_id=" + obj.channel_post.message_id, {
                    method: "GET",
                });
                var jsondata = await res.text();
                var copydata1 = JSON.parse(jsondata);
                if (res.ok) {
                    console.log("Incoming Message Copied.")
                    //await fetch("https://api.telegram.org/bot"+tg_bot_token+"/SendMessage?disable_web_page_preview=true&chat_id="+admin+"&text=RES OK: "+from_channel+" : "+jsondata, {
                    //    method: "GET",
                    //});
                    return new Response("OK!", {
                        status: 200,
                        headers: {
                            'content-type': 'application/json',
                        },
                    });
                } else if (res.status == 429) {
                    await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Flood Wait.\n\nRetry after " + copydata1.parameters.retry_after + " Seconds.", {
                        method: "GET",
                    });
                    return new Response(jsondata, {
                        status: 429,
                        headers: {
                            'content-type': 'application/json',
                        },
                    });
                } else {
                    await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Problem at Channel (not 429) " + from_channel + " : " + jsondata, {
                        method: "GET",
                    });
                    return new Response("Something Else", {
                        status: 200,
                        headers: {
                            'content-type': 'application/json',
                        },
                    });
                }
            }
        } else*/
        if (obj.hasOwnProperty('message')) {
            if (obj.message.hasOwnProperty('text')) {
                var usertext = obj.message.text
                if (usertext.startsWith('/from_message ')) {
                    usertext = usertext.substring(14);
                    await ENV.put("start_message_id", usertext);
                    await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=Starting Message ID: " + usertext + "\n\nSuccessfully Set.", {
                        method: "GET",
                    });
                } else if (usertext.startsWith('/to_message ')) {
                    usertext = usertext.substring(12);
                    await ENV.put("last_message_id", usertext);
                    await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=Ending Message ID: " + usertext + "\n\nSuccessfully Set.", {
                        method: "GET",
                    });
                } else if (usertext.startsWith('/from_channel ')) {
                    usertext = usertext.substring(14);
                    await ENV.put("from_channel", usertext);
                    await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=From Channel ID: " + usertext + "\n\nSuccessfully Set.", {
                        method: "GET",
                    });
                } else if (usertext.startsWith('/to_channel ')) {
                    usertext = usertext.substring(12);
                    await ENV.put("to_channel", usertext);
                    await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=To Channel ID: " + usertext + "\n\nSuccessfully Set.", {
                        method: "GET",
                    });
                } else if (usertext == '/startbot') {
                    if (from_channel != "" && to_channel != "" && first_message_id != "" && last_message != "" && first_message_id < last_message) {
                        if (status == "false") {
                            await ENV.put("status", "true");
                            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=Bot Started!\n\nFrom: " + from_channel + "\nFrom " + first_message_id + " To " + last_message + "\nTo: " + to_channel, {
                                method: "GET",
                            });
                        } else {
                            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=Bot already Started.", {
                                method: "GET",
                            });
                        }
                    } else {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=Incomplete Data.\nCheck All Values.\nTry /status", {
                            method: "GET",
                        });
                    }
                } else if (usertext == '/stopbot') {
                    if (status == "true") {
                        await ENV.put("status", "false");
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=Bot Stopped!", {
                            method: "GET",
                        });
                    } else {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=Bot already Stopped.", {
                            method: "GET",
                        });
                    }
                } else if (usertext == '/status') {
                    if (status == "true") {
                        var new_status = "Started"
                    } else {
                        var new_status = "Stopped"
                    }
                    if (from_channel != "" && to_channel != "" && first_message_id != "" && last_message != "" && first_message_id < last_message) {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=Bot Status: " + new_status + "\n\nFrom: " + from_channel + "\nTo: " + to_channel + "\n\nCurrently Cloning: " + first_message_id + " upto " + last_message, {
                            method: "GET",
                        });
                    } else {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=Bot Status: " + new_status + "\n\nFrom: " + from_channel + "\nTo: " + to_channel + "\nMessaged IDs: " + first_message_id + " to " + last_message, {
                            method: "GET",
                        });
                    }

                } else {
                    await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=Bhadoo Cloner Bot Private Server.", {
                        method: "GET",
                    });
                }
            }
        }
        //await fetch("https://api.telegram.org/bot"+tg_bot_token+"/SendMessage?disable_web_page_preview=true&chat_id="+obj.message.from.id+"&text="+data, {
        //    method: "GET",
        //});
        return new Response("OK", {
            status: 200,
            headers: {
                'content-type': 'application/json',
            },
        });
    } else {
        return new Response("OK", {
            status: 200,
            headers: {
                'content-type': 'application/json',
            },
        });
    }
}

addEventListener('fetch', event => {
    return event.respondWith(handleRequest(event.request));
});
